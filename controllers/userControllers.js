const User = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
// const { reset } = require('nodemon');
const Product = require('../models/Product');

// REGISTER USER
module.exports.registerUser = (req, res) => {
    console.log(req.body);

    User.findOne({ email: req.body.email }).then(result => {
        console.log(result);

        if (result !== null && result.email == req.body.email) {
            return res.send('Account is already registered')
        } else {
            const hashedPW = bcrypt.hashSync(req.body.password, 10)
            let newUser = new User({
                username : req.body.username,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: hashedPW,
            })

            newUser.save()
                .then(result => res.send(result))
                .catch(err => res.send(err))
        }
    })
}

//!! CHECK EMAIL EXISTS
module.exports.checkEmailExists = (req, res) => {
    console.log(req.body.email)
    User.find({ email: req.body.email })
        .then(result => {
            if (result.length === 0) {
                return res.send(false)
            } else {
                res.send(true)
            }
        })
        .catch((err) => res.send(err))
}


//!! GET ALL USERS
module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

//! LOG IN USER / USER AUTHENTICATION
module.exports.login = (req, res) => {
    console.log(req.body);
    User.findOne({username : req.body.username})
    .then(foundUser => {
        if(foundUser === null) {
            res.send(false)

        }else {
            const correctPassword = bcrypt.compareSync(req.body.password, foundUser.password)
            console.log(correctPassword);

            if(correctPassword){

                return res.send({accessToken: auth.createAccessToken(foundUser)})
            } else {
                return res.send(false)
            }
        }
    })
}


//! MAKE ADMIN
module.exports.makeAdmin = (req, res) => {
    console.log(req.user.id);
    console.log(req.params.id);

    let updates = {

        isAdmin: true
    }
    User.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(makeAdmin => res.send(makeAdmin))
        .catch(err => res.send(err));
};



//! GET SINGLE USER DETAILS [ADMIN]
module.exports.getSingleDetails = (req, res) => {
    console.log(req.body)
    console.log(req.user.id)

    User.findById(req.user.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

module.exports.order = async(req, res) => {
    console.log(req.user.id)
    console.log(req.body.productId)

    if (req.user.isAdmin) {
        return res.send(false)
    }

    let isUserUpdated = await User.findById(req.user.id).then(user => {



        let newProduct = {
            productId: req.body.productId,
            productName: req.body.productName,
            productDescription: req.body.productDescription,
            productPrice: req.body.productPrice
        }

        user.cart.push(newProduct);

        console.log(user)

        return user.save()
            .then(user => true)
            .catch(err => err.message)


    })


    if (isUserUpdated !== true) {
        return res.send({ message: isUserUpdated })
    }

    let isProductUpdated = await Product.findById(req.body.productId).then(product => {
        console.log(product)

        let newOrders = {
            userId : req.user.id
        }

        product.orders.push(newOrders)


        return product.save()
            .then(product => true)
            .catch(err => err.message)
    })

    if (isProductUpdated !== true) {
        return res.send({ message: isProductUpdated })
    }
    if (isUserUpdated && isProductUpdated) {
        return res.send({ message: true})
    }

};


// GET ORDERS

module.exports.getOrder = (req, res) =>{

    User.findById(req.user.id)
    .then(result => res.send(result.cart))
    .catch(err => res.send(err));
};

// TEST

module.exports.deleteOrder = async(req, res) =>{
    try{
        const id = req.params.id
        const orderId = req.params.order_id
        let user = await User.findById(req.user.id)


        user.cart = user.cart.filter(item =>item.id != orderId)
        console.log(user.cart)
        let newCart = await User.findByIdAndUpdate(req.user.id, user)

        console.log(user.cart)
        console.log(newCart.acknowledged)
    }
    catch(error){
        res.status(400).json({message: error.message})
    };
};

