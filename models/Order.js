const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    order: [{
        totalAmount: {
            type: Number,
            required: [true, "Total amount  is required"]
        },

        purchasedOn: {
            type: Date,
            default: new Date()
        },

        userID: {
            type: String,
            required: [true, "User ID  is required"]
        },

        productID: {
            type: String,
            required: [true, "Product Id  is required"]
        },

        orders: {
            type: String,
            required: [true, "Orders is required"]
        }
    }]
});

module.exports = mongoose.model("Order", orderSchema);
