const mongoose = require('mongoose');
const productSchema = mongoose.Schema({
    name: {
        type: String,
        required : true
    },
    description : {
        type: String,
        required : true
    },
    price : {
        type: Number,
        required : true

    },
    isActive : {
        type: Boolean,
        default : true
    },

    quantity : {
            type: Boolean,
            default: true
        },

    orders : [{
        userId : {
            type: String,
            required : true
        }

    }]

});

module.exports = mongoose.model ('Product', productSchema);